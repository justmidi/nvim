-- see `:help` for any questions
-- use `&<var>` to show value of vimscript variable


-- API --
o     = vim.o     -- options
go    = vim.go    -- only-global options
bo    = vim.bo    -- buffer local options
wo    = vim.wo    -- window local options

cmd   = vim.cmd   -- vim commands
fn    = vim.fn    -- vim functions
opt   = vim.opt   -- vim option object

g     = vim.g     -- global variables
b     = vim.b     -- buffer local variables
w     = vim.w     -- window local variables
t     = vim.t     -- tab local variables
v     = vim.v     -- variables
env   = vim.env   -- environment variables

local modules = {
    'ui',
    'general',
    'keymap',
    'plugins',
    'native-lsp',
    'indent-blankline',
    'gitsigns-nvim',
    'lightbulb'
}

local async
async = vim.loop.new_async(
    vim.schedule_wrap(
        function()
            for i = 1, #modules, 1 do
                pcall(require, modules[i])
            end
            async:close()
        end
    )
)
async:send()
