function bind(f,...)
    local args={...}
    return function(...)
        return f(unpack(args),...)
    end
end
