require "paq" {
    -- let paq manage itself
    "savq/paq-nvim";

    -- lsp
    "neovim/nvim-lspconfig";
    {"williamboman/nvim-lsp-installer", branch="dont-prepare-root-dir"};

    -- cmp
    "hrsh7th/nvim-cmp";
    "hrsh7th/cmp-nvim-lsp";
    "hrsh7th/cmp-buffer";
    "hrsh7th/cmp-path";
    "hrsh7th/cmp-cmdline";
    "hrsh7th/cmp-nvim-lua";
    "hrsh7th/cmp-calc";
    "hrsh7th/cmp-emoji";
    "hrsh7th/cmp-latex-symbols";
    "octaltree/cmp-look";
    {"tzachar/cmp-tabnine", run="./install.sh"};

    -- lua snips
    "L3MON4D3/LuaSnip";
    "saadparwaiz1/cmp_luasnip";

    -- diagnostics
    "folke/trouble.nvim";
    "kyazdani42/nvim-web-devicons";

    -- pandoc
    "vim-pandoc/vim-pandoc";

    -- zig ls
    "zigtools/zls";

    -- theme
    "rakr/vim-one";

    "mattn/emmet-vim";
    --{"lervag/vimtex", opt=true};

    -- tree sitter
    "nvim-treesitter/nvim-treesitter";
    "nvim-treesitter/nvim-treesitter-textobjects";
    "ray-x/cmp-treesitter";

    "lukas-reineke/indent-blankline.nvim";

    "kosayoda/nvim-lightbulb";

    "turbio/bracey.vim";

    -- git symbols
    "lewis6991/gitsigns.nvim";
    "nvim-lua/plenary.nvim";
}

cmd 'PaqSync'
