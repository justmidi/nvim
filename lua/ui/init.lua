-- see `:help` for any questions
-- use `&<var>` to show value of vimscript variable

require('ui/statusbar')

-- API --
-- o     = vim.o     -- options
-- go    = vim.go    -- only-global options
-- bo    = vim.bo    -- buffer local options
-- wo    = vim.wo    -- window local options

-- cmd   = vim.cmd   -- vim commands
-- fn    = vim.fn    -- vim functions
-- opt   = vim.opt   -- vim option object

-- g     = vim.g     -- global variables
-- b     = vim.b     -- buffer local variables
-- w     = vim.w     -- window local variables
-- t     = vim.t     -- tab local variables
-- v     = vim.v     -- variables
-- env   = vim.env   -- environment variables



o.showcmd       = true
wo.cursorline   = true
cmd 'filetype plugin indent on'
o.lazyredraw    = true
o.showmatch     = true  -- matching bracket


-- Completion Menu
--o.wildmenu  = true
--o.wildmode  = 'list:longest,full'


-- Character Representation
o.sbr   = '…' -- line wrap character
o.list  = true
opt.listchars = { -- white space representation
    tab         = '▸ ',
    trail       = '•',
    nbsp        = '␣',
    extends     = '⟩',
    precedes    = '⟨'
}


-- Hybrid Numbers
function hybrid_numbers(x)
    wo.number           = true
    wo.relativenumber   = x
end
cmd([[
autocmd BufEnter,FocusGained,InsertLeave * lua hybrid_numbers(true)
autocmd BufLeave,FocusLost,InsertEnter * lua hybrid_numbers(false)
]])


-- Search
o.incsearch = true  -- show matching patterns
o.hlsearch  = true  -- highlight all patterns


-- Status Bar
o.laststatus = 2 -- always visible
vim.o.statusline = "%!luaeval('status_bar()')"


-- Tabs
local indent_size = 4 --change to filetype
opt.expandtab   = true  -- tab key inserts spaces
opt.tabstop     = indent_size
opt.shiftwidth  = indent_size
opt.softtabstop = 0     -- to disable
opt.smarttab    = true

--adjust to filetype
function no_ro_retab()
    if vim.inspect(opt.readonly:get()) == false then
        cmd '%retab'
    end
end
cmd 'autocmd BufReadPost,BufWritePre,BufWritePost,BufNewFile * lua no_ro_retab()'


-- Text Width
wo.wrap         = true
wo.linebreak    = true
bo.textwidth    = 70

bo.formatoptions = table.concat({
    'j',    -- remove comment when joining lines
    'w',    -- non-white space at end of line means end of paragraph
    'c',    -- insert comment when auto wrapping textwidth
    'r',    -- insert comment after hitting <enter>
    'o',    -- insert comment after hitting "o"
    'q',    -- format comments with "gq"
    'l'     -- long lines are not broken in insert mode
})

cmd([[
autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#592929
autocmd BufEnter * match OverLength /\%71v.*/
]]) --rewrite in lua

-- Theme
o.background = "light"
cmd('colorscheme one')

--WIP
-- Filetype: txt, md, tex
-- break lines, autowrap, autoformat, recognize numbered lists
--fix before enabling
--autocmd BufRead,BufNewFile *.md,*.txt,*.tex set fo-=l
--autocmd BufRead,BufNewFile *.md,*.txt,*.tex set fo+=tan
